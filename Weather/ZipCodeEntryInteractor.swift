//
//  ZipCodeEntryInteractor.swift
//  Weather
//
//  Created by Joey LaBarck on 12/4/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import Foundation
import WeatherSDK

protocol ZipCodeEntryInteractorDelegate: class {
    func didReceiveOutput(_ output: ZipCodeEntryInteractor.Output)
}

class ZipCodeEntryInteractor: WeatherStoreDelegate {
    
    enum Input {
        case zipCode(String)
    }
    
    enum Output {
        case present(UIViewController)
    }
    
    private weak var delegate: ZipCodeEntryInteractorDelegate?
    private lazy var store: WeatherStore = {
       return WeatherStore(delegate: self)
    }()
    private var lastZipCodeEntered: String? = nil
    
    init(delegate: ZipCodeEntryInteractorDelegate) {
        self.delegate = delegate
    }
    
    func input(_ input: ZipCodeEntryInteractor.Input) {
        
        switch input {
        case let .zipCode(zipCode):
            self.lastZipCodeEntered = zipCode
            self.store.refresh(forZipCode: zipCode)
        }
    }
    
    // MARK: WeatherStoreDelegate
    func didReceiveWeatherUpdate() {
        
        // Grab the name
        let alert = UIAlertController(title: "Weather Update For \(self.store.city)", message: self.store.description, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(ok)
        
        self.delegate?.didReceiveOutput(.present(alert))
    }
    
    func didFailToReceiveWeatherUpdate() {
        
        let alert = UIAlertController(title: "Request Failed", message: "Try again?", preferredStyle: .alert)
        let nah = UIAlertAction(title: "Nah", style: .cancel, handler: nil)
        let sure = UIAlertAction(title: "Sure", style: .default, handler: { _ in
            guard let zip = self.lastZipCodeEntered else { return }
            self.store.refresh(forZipCode: zip)
        })
        alert.addAction(nah)
        alert.addAction(sure)
        
        self.delegate?.didReceiveOutput(.present(alert))
    }
}
