//
//  ZipCodeInputViewController.swift
//  Weather
//
//  Created by Joey LaBarck on 12/4/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import UIKit

class ZipCodeInputViewController: UIViewController, UITextFieldDelegate, ZipCodeEntryInteractorDelegate {
    
    @IBOutlet weak var zipCodeTextField: UITextField!
    
    private lazy var interactor: ZipCodeEntryInteractor = {
        return ZipCodeEntryInteractor(delegate: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.zipCodeTextField.delegate = self
    }
    
    // MARK: ZipCodeEntryInteractorDelegate
    func didReceiveOutput(_ output: ZipCodeEntryInteractor.Output) {
        
        switch output {
        case let .present(viewController):
            DispatchQueue.main.async {
                self.present(viewController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        guard let zipCode = textField.text else { return false }
        guard zipCode.characters.count == 5 else { return false }
        
        interactor.input(.zipCode(zipCode))
        
        return true
    }
}
