//
//  Networking.swift
//  WeatherSDK
//
//  Created by Joey LaBarck on 12/4/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
}

enum NetworkError: Error {
    case error(Error)
    case badRequest
    case noResponse
    case noData
    case jsonDeserializationFailure
}

protocol URLRequestConvertible {
    
    var host: String { get }
    var path: String { get}
    var headers: [String: String]? { get }
    var request: URLRequest? { get }
}

protocol POSTURLRequestConvertible: URLRequestConvertible {
    var body: Data? { get }
}

protocol GETURLRequestConvertible: URLRequestConvertible {
    var queryParameters: [String: String]? { get }
}

extension URLRequestConvertible {
    
    var headers: [String: String]? {
        return nil
    }
    
    var request: URLRequest? {

        guard var components = URLComponents(string: "http://" + host + path) else { return nil }
        components.queryItems = (self as? GETURLRequestConvertible)?.queryParameters?.map({ URLQueryItem(name: $0, value:$1) })
        
        guard let url = components.url else { return nil }
        
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = headers
        request.httpBody = (self as? POSTURLRequestConvertible)?.body
        return request
    }
}

struct Server: _Server {}

protocol _Server {
    
    static var server: URLSession { get }
    
    static func dataTask<T: Decodable>(_ request: URLRequestConvertible, _ completion: @escaping (Result<T>) -> Void)
}

extension _Server {
    
    static var server: URLSession {
        return URLSession(configuration: .ephemeral, delegate: TLSPublicKeyPinning(), delegateQueue: nil)
    }
}

extension _Server {
    
    static func dataTask<T: Decodable>(_ request: URLRequestConvertible, _ completion: @escaping (Result<T>) -> Void) {
        
        guard let request = request.request else { return }
        
        self.server.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                print("There was an error")
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                print("There was no response from the server")
                return
            }
            
            guard let data = data else {
                print("There was no data returned from the server")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let object = try decoder.decode(T.self, from: data)
                completion(.success(object))
            } catch {
                completion(.failure(error))
            }
            
        }.resume()
    }
}
