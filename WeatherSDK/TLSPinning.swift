//
//  TLSPinning.swift
//  WeatherSDK
//
//  Created by Joey LaBarck on 12/8/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import Foundation

class TLSPinning: NSObject, URLSessionDelegate {
    
    final func evaluateServerTrust(_ trust: SecTrust) -> Bool {
        return false
    }
    
    // MARK: URLSessionDelegate
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        fatalError("Must be overidden.")
    }
}

// In a sep. file, overrides for specific tls algorithms
final class TLSPublicKeyPinning: TLSPinning {
    
    // methods for fetching pinned certs/keys & extracting certs/keys from trust object
    
    override func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
     
        // Domain specific logic goes here...
    }
}

final class TLSCertificateAuthorityPinning: TLSPinning {
    
    // methods for fetching pinned anchors for trust evaluation
    
    override func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        //
    }
}
