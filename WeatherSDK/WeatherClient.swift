//
//  WeatherClient.swift
//  WeatherSDK
//
//  Created by Joey LaBarck on 12/8/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import Foundation

struct WeatherClient {
    
    static func receiveWeather(server: _Server.Type = Server.self, forZipCode zipCode: String, countryCode: String = "us", _ completion: @escaping (Result<WeatherResponse>) -> Void) {
        server.dataTask(WeatherRequest(zipCode: zipCode, countryCode: countryCode), completion)
    }
}
