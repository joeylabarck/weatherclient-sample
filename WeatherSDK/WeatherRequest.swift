//
//  WeatherRequest.swift
//  WeatherSDK
//
//  Created by Joey LaBarck on 12/4/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import Foundation

struct WeatherRequest: GETURLRequestConvertible {
    
    private let zipCode: String
    private let countryCode: String
    
    init(zipCode: String, countryCode: String = "us") {
        self.zipCode = zipCode
        self.countryCode = countryCode
    }
    
    // MARK: GETURLRequestConvertible
    var host: String = "api.openweathermap.org"
    var path: String = "/data/2.5/weather"
    var queryParameters: [String : String]? {
        return ["zip": "\(self.zipCode),\(self.countryCode)",
            "APPID": "74d49b0dbaa5b64064f2943a2087efe0"]
    }
}
