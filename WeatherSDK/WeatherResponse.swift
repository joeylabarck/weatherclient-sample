//
//  WeatherResponse.swift
//  WeatherSDK
//
//  Created by Joey LaBarck on 12/4/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import Foundation

struct WeatherResponse: Decodable {
    
    private(set) var weather: [WeatherItem]?
    private(set) var main: Main?
    private(set) var wind: Wind?
    private(set) var name: String?
    
    struct WeatherItem: Decodable {
        private(set) var main: String?
        private(set) var description: String?
    }
    
    struct Main: Decodable {
        private(set) var temp: Float?
        private(set) var temp_min: Float?
        private(set) var temp_max: Float?
    }
    
    struct Wind: Decodable {
        private(set) var speed: Float?
    }
}
