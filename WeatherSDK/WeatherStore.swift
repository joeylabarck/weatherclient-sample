//
//  WeatherStore.swift
//  WeatherSDK
//
//  Created by Joey LaBarck on 12/4/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import Foundation

public protocol WeatherStoreDelegate: class {
    func didReceiveWeatherUpdate()
    func didFailToReceiveWeatherUpdate()
}

public class WeatherStore {
    
    private weak var delegate: WeatherStoreDelegate?
    
    public init(delegate: WeatherStoreDelegate) {
        self.delegate = delegate
    }
    
    public func refresh(forZipCode zipCode: String, countryCode: String = "us") {
        
        WeatherClient.receiveWeather(forZipCode: zipCode) { response in
            
            switch response {
                
            case let .success(update):
                
                self.city = update.name ?? "New York City"
                self.description = update.weather?.first?.description ?? "IT'S GONNA RAIN"
                
                self.delegate?.didReceiveWeatherUpdate()
                
            case .failure:
                self.delegate?.didFailToReceiveWeatherUpdate()
            }
        }
    }
    
    public private(set) var city: String = "New York"
    public private(set) var description: String = "IT'S GONNA RAIN"
}
