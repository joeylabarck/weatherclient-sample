//
//  WeatherSDKTests.swift
//  WeatherSDKTests
//
//  Created by Joey LaBarck on 12/4/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import XCTest
@testable import WeatherSDK

class MockURLProtocol: URLProtocol {
    
    override class func canInit(with request: URLRequest) -> Bool {
        return false
    }
}

struct MockServer: _Server {
    
    static var server: URLSession {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses?.insert(MockURLProtocol.self, at: 0)
        return URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
    }
}

class WeatherSDKTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testWeatherRequest() {
        
        let success = expectation(description: "Success")
        
        WeatherClient.receiveWeather(server: MockServer.self, forZipCode: "07419", countryCode: "US", { response in

            success.fulfill()
        })
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
}
